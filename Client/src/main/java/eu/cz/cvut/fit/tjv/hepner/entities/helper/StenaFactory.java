/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.tjv.hepner.entities.helper;

import eu.cz.cvut.fit.tjv.hepner.entities.Stena;

/**
 *
 * @author heppy
 */
public class StenaFactory {
    private Stena stena;

    public StenaFactory(String city, String street, int streetNumber) {
        stena = new Stena();
        stena.setCity(city);
        stena.setStreet(street);
        stena.setStreetNumber(streetNumber);
    }
    
    public StenaFactory(String city, String street, int streetNumber, Long id) {
        stena = new Stena();
        stena.setCity(city);
        stena.setStreet(street);
        stena.setStreetNumber(streetNumber);
        stena.setId(id);
    }

    public Stena getStena() {
        return stena;
    }
    
}
