package eu.cz.cvut.fit.tjv.hepner.entities.helper;

import eu.cz.cvut.fit.tjv.hepner.entities.Lano;
import eu.cz.cvut.fit.tjv.hepner.entities.Stena;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author heppy
 */
public class Lana {
    private List<Lano> lana = new ArrayList<>();

    public Lana() {
    }
    
    public List<Lano> getLana(Stena s) {
        List<Lano> res = new ArrayList<>();
        for (Lano l : lana) {
            if (l.getWall().equals(s)) {
                res.add(l);
            }
        }
        return res;
    }
    
    public List<Lano> search(String query) {
        int numInt = -1;
        double numDouble = -1;
        try {
                numInt = Integer.parseInt(query);
            }
        catch (Exception e) {}
        try {
                numDouble = Double.parseDouble(query);
            }
        catch (Exception e) {}
        List<Lano> res = new ArrayList<>();
        
        for (Lano l : lana) {
            if (numInt != -1) {
                if (l.getId() == numInt || l.getLenght() == numInt || l.getWall().getId() == numInt)
                    res.add(l);
            }
            
            if (numDouble != -1) {
                if (l.getShift() == numDouble || l.getWidth() == numDouble)
                    res.add(l);
            }
        }
        
        return res;
    }
    
    public Lana(List<Lano> lana) {
        this.lana = lana;
    }

    public List<Lano> getLana() {
        return lana;
    }

    public void setLana(List<Lano> lana) {
        this.lana = lana;
    }
    
}
