/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.tjv.hepner.entities.helper;

import eu.cz.cvut.fit.tjv.hepner.entities.Stena;
import eu.cz.cvut.fit.tjv.hepner.entities.Trener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author heppy
 */
public class Steny {
    private List<Stena> steny = new ArrayList<Stena>();

    public Steny() {
    }
    
    public Steny(List<Stena> steny) {
        this.steny = steny;
    }
    
    public List<Stena> getSteny(Trener trener) {
        List<Stena> res = new ArrayList<>();
        for (Stena s : steny) {
            if (s.getTreners().contains(trener))
                res.add(s);
        }
        return res;
    }
    
    public List<Stena> search(String query) {
        List<Stena> res = new ArrayList<>();
        for (Stena s : steny) {
            if (s.getCity().toLowerCase().contains(query.toLowerCase()) ||
                s.getStreet().toLowerCase().contains(query.toLowerCase()) ) {
                res.add(s);
            }
            try {
                int num = Integer.parseInt(query);
                if (s.getStreetNumber() == num || s.getId() == num)
                    res.add(s);
            }
            catch (Exception e) {}
        }
        
        return res;
    }
    
    public List<Stena> getSteny() {
        return steny;
    }

    public void setSteny(List<Stena> steny) {
        this.steny = steny;
    }
    
}
