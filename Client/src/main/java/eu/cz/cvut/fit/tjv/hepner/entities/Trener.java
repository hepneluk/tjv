/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.tjv.hepner.entities;

import java.io.Serializable;
import java.util.List;


/**
 *
 * @author heppy
 */

public class Trener implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    
    private String firstName;
    private String surname;
    private String email;
    

    private List<Stena> trainsAt;
    
    public void AddStena(Stena s) {
        if (trainsAt.contains(s))
            return;
        trainsAt.add(s);
    }
    
    public void RemoveStena(Stena s) {
        trainsAt.remove(s);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    public List<Stena> getTrainsAt() {
        return trainsAt;
    }

    public void setTrainsAt(List<Stena> trainsAt) {
        this.trainsAt = trainsAt;
    }
  
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trener)) {
            return false;
        }
        Trener other = (Trener) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.cz.cvut.fit.tjv.hepner.semestralkaserver.entities.Trener[ id=" + id + " ]";
    }
    
}
