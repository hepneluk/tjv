/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.tjv.hepner.entities.helper;

import eu.cz.cvut.fit.tjv.hepner.entities.Stena;
import eu.cz.cvut.fit.tjv.hepner.entities.Trener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author heppy
 */
public class Treneri {
    private List<Trener> treneri = new ArrayList<Trener>();

    public Treneri() {
    }
    
    public Treneri(List<Trener> treneri) {
        this.treneri = treneri;
    }
    
    public List<Trener> getTreneri(Stena stena) {
        List<Trener> res = new ArrayList<>();
        for (Trener t : treneri) {
            if (t.getTrainsAt().contains(stena))
                res.add(t);
        }
        return res;
    }

    public List<Trener> getTreneri() {
        return treneri;
    }

    public void setTreneri(List<Trener> treneri) {
        this.treneri = treneri;
    }
    
    public List<Trener> search(String query) {
        List<Trener> res = new ArrayList<>();
        for (Trener t : treneri) {
            if (t.getFirstName().toLowerCase().contains(query.toLowerCase()) ||
                t.getSurname().toLowerCase().contains(query.toLowerCase()) ||
                t.getEmail().toLowerCase().contains(query.toLowerCase()) ) {
                res.add(t);
            }
        }
        
        return res;
    }
}
