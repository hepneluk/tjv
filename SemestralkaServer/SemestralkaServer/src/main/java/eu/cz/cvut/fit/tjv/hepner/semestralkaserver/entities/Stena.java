package eu.cz.cvut.fit.tjv.hepner.semestralkaserver.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author heppy
 */
@Entity
@XmlRootElement
@Table(name = "STENA")
public class Stena implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String city;
    private String street;
    private int streetNumber;
    
    @ManyToMany(mappedBy = "trainsAt")
    private List<Trener> treners;
    
    @OneToMany(mappedBy = "wall", cascade = CascadeType.REMOVE)
    private List<Lano> ropes;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    @XmlTransient
    public List<Trener> getTreners() {
        return treners;
    }

    public void setTreners(List<Trener> treners) {
        this.treners = treners;
    }

    @XmlTransient
    public List<Lano> getRopes() {
        return ropes;
    }

    public void setRopes(List<Lano> ropes) {
        this.ropes = ropes;
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stena)) {
            return false;
        }
        Stena other = (Stena) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.cz.cvut.fit.tjv.hepner.semestralkaserver.entities.Stena[ id=" + id + " ]";
    }
    
}
