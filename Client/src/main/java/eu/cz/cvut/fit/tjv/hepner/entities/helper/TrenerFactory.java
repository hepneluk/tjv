/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.tjv.hepner.entities.helper;

import eu.cz.cvut.fit.tjv.hepner.entities.Trener;

/**
 *
 * @author heppy
 */
public class TrenerFactory {
    private Trener trener;
    
    public TrenerFactory(Trener trener){
        this.trener = trener;
    }
    
    public TrenerFactory(String firstName, String surname, String email) {
        trener = new Trener();
        trener.setFirstName(firstName);
        trener.setSurname(surname);
        trener.setEmail(email);
    }
    
    public TrenerFactory(String firstName, String surname, String email, Long id) {
        trener = new Trener();
        trener.setFirstName(firstName);
        trener.setSurname(surname);
        trener.setEmail(email);
        trener.setId(id);
    }

    public Trener getTrener() {
        return trener;
    }
}
