package eu.cz.cvut.fit.tjv.hepner.client;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import eu.cz.cvut.fit.tjv.hepner.entities.Stena;
import eu.cz.cvut.fit.tjv.hepner.entities.helper.TrenerFactory;
import eu.cz.cvut.fit.tjv.hepner.entities.Trener;
import eu.cz.cvut.fit.tjv.hepner.entities.Lano;
import eu.cz.cvut.fit.tjv.hepner.entities.helper.Lana;
import eu.cz.cvut.fit.tjv.hepner.entities.helper.LanoFactory;
import eu.cz.cvut.fit.tjv.hepner.entities.helper.StenaFactory;
import java.util.Set;
import javax.ws.rs.ClientErrorException;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    private final LanoClient lanoClient = new LanoClient();
    private final StenaClient stenaClient = new StenaClient();
    private final TrenerClient trenerClient = new TrenerClient();
    
    private Button btnHome = new Button("Zpět");
    
    private Trener lastTrener = null;

    @Override
    protected void init(VaadinRequest vaadinRequest) {        
        btnHome.addClickListener( e -> {
            MainScreen();
        });
        
        
        MainScreen();
    }
    
    private void Trener() {  
        VerticalLayout layoutTrener = new VerticalLayout();
        HorizontalLayout layoutHome = new HorizontalLayout(btnHome);
        HorizontalLayout layoutQuery = new HorizontalLayout();
        
        VerticalLayout layoutAdd = new VerticalLayout();
        VerticalLayout layoutSearch = new VerticalLayout();
        VerticalLayout layoutEdit = new VerticalLayout();
        HorizontalLayout layoutGrid = new HorizontalLayout();
        HorizontalLayout layoutDelete = new HorizontalLayout();
        
        Grid<Trener> gridTreners = new Grid<>();
        Grid<Stena> gridSteny = new Grid<>();
        
        // add trener
        TextField txtFirstname = new TextField("Jméno:");
        TextField txtSurname = new TextField("Příjmení:");
        TextField txtEmail = new TextField("Email:");
        
        Button btnAdd = new Button("Přidej trenéra");
        
        btnAdd.addClickListener( e -> {
            CreateTrener(txtFirstname.getValue(), txtSurname.getValue(), txtEmail.getValue());
            gridTreners.setItems(trenerClient.findAll().getTreneri());
        });
        
        layoutAdd.addComponents(txtFirstname, txtSurname, txtEmail, btnAdd);
        layoutQuery.addComponent(layoutAdd);
        
        // search for trener
        TextField txtSearch = new TextField("Najdi trenéra:");
        Button btnSearch = new Button("Hledej");
        
        btnSearch.addClickListener( e -> {
            System.out.println("\"" + txtSearch.getValue() + "\"");
            gridTreners.setItems(trenerClient.findAll().search(txtSearch.getValue()));
        });
        layoutSearch.addComponents(txtSearch, btnSearch);
        layoutQuery.addComponent(layoutSearch);
        
        // edit trener
        TextField txtEditId = new TextField("ID:");
        txtEditId.setReadOnly(true);
        TextField txtEditFirstname = new TextField("Jméno:");
        TextField txtEditSurname = new TextField("Příjmení:");
        TextField txtEditEmail = new TextField("Email:");
        TextField txtEditWall = new TextField("Stěna:");
        
        Button btnEdit = new Button("Uprav trenéra");
        
        btnEdit.addClickListener( e -> {
            EditTrener(txtEditId.getValue(),
                       txtEditFirstname.getValue(),
                       txtEditSurname.getValue(),
                       txtEditEmail.getValue(),
                       txtEditWall.getValue());
            gridTreners.setItems(trenerClient.findAll().getTreneri());
        });
        
        layoutEdit.addComponents(txtEditId, txtEditFirstname, txtEditSurname, txtEditEmail, txtEditWall, btnEdit);
        layoutQuery.addComponent(layoutEdit);
        
        // delete
        Button btnDelete = new Button("Smaž vybrané");
        btnDelete.addListener(e -> {
            Set<Trener> selected = gridTreners.getSelectedItems();
            for (Trener t : selected) {
                trenerClient.remove(t.getId().toString());
            }
            gridTreners.setItems(trenerClient.findAll().getTreneri());
        });
        
        Button btnDeleteWall = new Button("Smaž stěnu");
        btnDeleteWall.addListener(e -> {
            Set<Stena> selected = gridSteny.getSelectedItems();
            for (Stena s : selected) {
                RemoveWall(s);
            }
            gridTreners.setItems(trenerClient.findAll().getTreneri());
        });
        
        // show all
        Button btnAll = new Button("Ukaž vše");
        btnAll.addClickListener( e -> {
            gridTreners.setItems(trenerClient.findAll().getTreneri());
            gridTreners.setCaption("Trenéři");
            gridSteny.setItems(stenaClient.findAll().getSteny());
            gridSteny.setCaption("Stěny");
            ChangeSelected(null);
        });
        
        // grid of treners
        
        gridTreners.setSelectionMode(Grid.SelectionMode.MULTI);
        
        gridTreners.setCaption("Trenéři");
        gridTreners.addColumn(Trener::getId).setCaption("ID");
        gridTreners.addColumn(Trener::getFirstName).setCaption("Jméno");
        gridTreners.addColumn(Trener::getSurname).setCaption("Příjmení");
        gridTreners.addColumn(Trener::getEmail).setCaption("Email");
        gridTreners.setItems(trenerClient.findAll().getTreneri());
        
        
        gridTreners.addItemClickListener(e -> {
            txtEditFirstname.setValue(e.getItem().getFirstName());
            txtEditSurname.setValue(e.getItem().getSurname());
            txtEditEmail.setValue(e.getItem().getEmail());
            txtEditId.setValue(e.getItem().getId().toString());
            ChangeSelected(e.getItem());
            
            gridSteny.setItems(e.getItem().getTrainsAt());
            gridSteny.setCaption("Trénuje na");
        });
        
        // grid of stena
        gridSteny.setSelectionMode(Grid.SelectionMode.MULTI);
        gridSteny.setCaption("Stěny");
        gridSteny.addColumn(Stena::getId).setCaption("ID");
        gridSteny.addColumn(Stena::getCity).setCaption("City");
        gridSteny.addColumn(Stena::getStreet).setCaption("Street");
        gridSteny.addColumn(Stena::getStreetNumber).setCaption("Number");
        gridSteny.setItems(stenaClient.findAll().getSteny());
        
        gridSteny.addItemClickListener(e -> {
            txtEditWall.setValue(e.getItem().getId().toString());
        });

        layoutGrid.addComponents(gridTreners, gridSteny);

        // add components
        layoutDelete.addComponents(btnDelete, btnAll, btnDeleteWall);
        layoutTrener.addComponents(layoutHome, layoutQuery, layoutDelete, layoutGrid);
               
        setContent(layoutTrener);
    }
    
    private void Stena() {
//        lanoClient.create_JSON(new LanoFactory(60, 1.7, 0.6,
//        stenaClient.findAll().getSteny().get(0)
//        ).getLano());
            
        VerticalLayout layoutStena = new VerticalLayout();
        HorizontalLayout layoutHome = new HorizontalLayout(btnHome);
        HorizontalLayout layoutQuery = new HorizontalLayout();
        
        VerticalLayout layoutAdd = new VerticalLayout();
        VerticalLayout layoutSearch = new VerticalLayout();
        VerticalLayout layoutEdit = new VerticalLayout();
        HorizontalLayout layoutGrid = new HorizontalLayout();
        HorizontalLayout layoutDelete = new HorizontalLayout();
        
        Grid<Stena> gridSteny = new Grid<>();
        Grid<Trener> gridTreners = new Grid<>();
        Grid<Lano> gridRopes = new Grid<>();
        
        // add stena
        TextField txtCity = new TextField("Město:");
        TextField txtStreet = new TextField("Ulice:");
        TextField txtNumber = new TextField("Číslo popisné:");
        
        Button btnAdd = new Button("Přidej stěnu");
        
        btnAdd.addClickListener( e -> {
            CreateWall(txtCity.getValue(), txtStreet.getValue(), txtNumber.getValue());
            gridSteny.setItems(stenaClient.findAll().getSteny());
        });
        
        layoutAdd.addComponents(txtCity, txtStreet, txtNumber, btnAdd);
        layoutQuery.addComponent(layoutAdd);
        
        // search for stena
        TextField txtSearch = new TextField("Najdi stěnu:");
        Button btnSearch = new Button("Hledej");
        
        btnSearch.addClickListener( e -> {
            System.out.println("\"" + txtSearch.getValue() + "\"");
            gridSteny.setItems(stenaClient.findAll().search(txtSearch.getValue()));
        });
        layoutSearch.addComponents(txtSearch, btnSearch);
        layoutQuery.addComponent(layoutSearch);
        
        // edit stena
        TextField txtEditId = new TextField("ID:");
        txtEditId.setReadOnly(true);
        TextField txtEditCity = new TextField("Město:");
        TextField txtEditStreet = new TextField("Ulice:");
        TextField txtEditNumber = new TextField("Číslo popisné:");
        
        Button btnEdit = new Button("Uprav stěnu");
        
        btnEdit.addClickListener( e -> {
            EditStena(txtEditId.getValue(),
                       txtEditCity.getValue(),
                       txtEditStreet.getValue(),
                       txtEditNumber.getValue());
            gridSteny.setItems(stenaClient.findAll().getSteny());
        });
        
        layoutEdit.addComponents(txtEditId, txtEditCity, txtEditStreet, txtEditNumber, btnEdit);
        layoutQuery.addComponent(layoutEdit);
        
        // delete
        Button btnDelete = new Button("Smaž vybrané");
        btnDelete.addListener(e -> {
            System.out.println("Delete");
            Set<Stena> selected = gridSteny.getSelectedItems();
            for (Stena s : selected) {
                for (Lano l : lanoClient.findAll().getLana(s)) {
                    System.out.println(l);
                    lanoClient.remove(l.getId().toString());
                }
                stenaClient.remove(s.getId().toString());
            }
            gridSteny.setItems(stenaClient.findAll().getSteny());
            gridTreners.setItems(trenerClient.findAll().getTreneri());
            gridRopes.setItems(lanoClient.findAll().getLana());
        });
        
        // show all
        Button btnAll = new Button("Ukaž vše");
        btnAll.addClickListener( e -> {
            gridTreners.setCaption("Trenéři");
            gridTreners.setItems(trenerClient.findAll().getTreneri());
            gridSteny.setItems(stenaClient.findAll().getSteny());
            gridRopes.setItems(lanoClient.findAll().getLana());
        });
        
        // grid of treners
        
        gridSteny.setSelectionMode(Grid.SelectionMode.MULTI);
        
        gridSteny.setCaption("Stěny");
        gridSteny.addColumn(Stena::getId).setCaption("ID");
        gridSteny.addColumn(Stena::getCity).setCaption("Město");
        gridSteny.addColumn(Stena::getStreet).setCaption("Ulice");
        gridSteny.addColumn(Stena::getStreetNumber).setCaption("Číslo popisné");
        gridSteny.setItems(stenaClient.findAll().getSteny());
        
        gridSteny.addItemClickListener(e -> {
            txtEditCity.setValue(e.getItem().getCity());
            txtEditStreet.setValue(e.getItem().getStreet());
            txtEditNumber.setValue(String.valueOf(e.getItem().getStreetNumber()));
            txtEditId.setValue(e.getItem().getId().toString());
            
            gridTreners.setCaption("Zde trénuje");
            gridTreners.setItems(trenerClient.findAll().getTreneri(e.getItem()));
            gridRopes.setItems(lanoClient.findAll().getLana(e.getItem()));
        });
        
        // grid of treners
        gridTreners.setCaption("Trenéři");
        gridTreners.addColumn(Trener::getId).setCaption("ID");
        gridTreners.addColumn(Trener::getFirstName).setCaption("Jméno");
        gridTreners.addColumn(Trener::getSurname).setCaption("Příjmení");
        gridTreners.addColumn(Trener::getEmail).setCaption("Email");
        
        // grid of ropes
        gridRopes.setCaption("Lana");
        gridRopes.addColumn(Lano::getId).setCaption("ID");
        gridRopes.addColumn(Lano::getLenght).setCaption("Délka");
        gridRopes.addColumn(Lano::getWidth).setCaption("Tloušťka");
        gridRopes.addColumn(Lano::getShift).setCaption("Posuv");
        
        layoutGrid.addComponents(gridSteny, gridTreners, gridRopes);

        // add components
        layoutDelete.addComponents(btnDelete, btnAll);
        layoutStena.addComponents(layoutHome, layoutQuery, layoutDelete, layoutGrid);
               
        setContent(layoutStena);
    }
        
    private void Lano() {
        VerticalLayout layoutLano = new VerticalLayout();
        HorizontalLayout layoutHome = new HorizontalLayout(btnHome);
        HorizontalLayout layoutQuery = new HorizontalLayout();

        VerticalLayout layoutAdd = new VerticalLayout();
        VerticalLayout layoutSearch = new VerticalLayout();
        VerticalLayout layoutEdit = new VerticalLayout();
        HorizontalLayout layoutGrid = new HorizontalLayout();
        HorizontalLayout layoutDelete = new HorizontalLayout();

        Grid<Lano> gridRopes = new Grid<>();
        Grid<Stena> gridSteny = new Grid<>();

        // add lano
        TextField txtLen = new TextField("Délka:");
        TextField txtWidth = new TextField("Šířka:");
        TextField txtShift = new TextField("Posuv opletu:");
        TextField txtWall = new TextField("ID stěny:");

        Button btnAdd = new Button("Přidej lano");

        btnAdd.addClickListener( e -> {
            CreateLano(txtLen.getValue(), txtWidth.getValue(), txtShift.getValue(), txtWall.getValue());
            gridRopes.setItems(lanoClient.findAll().getLana());
        });

        layoutAdd.addComponents(txtLen, txtWidth, txtShift, txtWall, btnAdd);
        layoutQuery.addComponent(layoutAdd);

        // search for lano
        TextField txtSearch = new TextField("Najdi lano:");
        Button btnSearch = new Button("Hledej");

        btnSearch.addClickListener( e -> {
            gridRopes.setItems(lanoClient.findAll().search(txtSearch.getValue()));
        });
        layoutSearch.addComponents(txtSearch, btnSearch);
        layoutQuery.addComponent(layoutSearch);

        // edit lano
        TextField txtEditId = new TextField("ID:");
        txtEditId.setReadOnly(true);
        TextField txtEditLen = new TextField("Délka:");
        TextField txtEditWid = new TextField("Šířka:");
        TextField txtEditShift = new TextField("Posun opletu:");
        TextField txtEditWall = new TextField("Stěna:");

        Button btnEdit = new Button("Uprav lano");

        btnEdit.addClickListener( e -> {
            EditLano(txtEditId.getValue(),
                       txtEditLen.getValue(),
                       txtEditWid.getValue(),
                       txtEditShift.getValue(),
                       txtEditWall.getValue());
            gridRopes.setItems(lanoClient.findAll().getLana());
        });

        layoutEdit.addComponents(txtEditId, txtEditLen, txtEditWid, txtEditShift, txtEditWall, btnEdit);
        layoutQuery.addComponent(layoutEdit);

        // delete
        Button btnDelete = new Button("Smaž vybrané");
        btnDelete.addListener(e -> {
            Set<Lano> selected = gridRopes.getSelectedItems();
            for (Lano l : selected) {
                lanoClient.remove(l.getId().toString());
            }
            gridRopes.setItems(lanoClient.findAll().getLana());
        });

        // show all
        Button btnAll = new Button("Ukaž vše");
        btnAll.addClickListener( e -> {
            gridRopes.setItems(lanoClient.findAll().getLana());
            gridSteny.setItems(stenaClient.findAll().getSteny());
        });

        // grid of treners

        gridRopes.setSelectionMode(Grid.SelectionMode.MULTI);

        gridRopes.addColumn(Lano::getId).setCaption("ID");
        gridRopes.addColumn(Lano::getLenght).setCaption("Déla");
        gridRopes.addColumn(Lano::getWidth).setCaption("Šířka");
        gridRopes.addColumn(Lano::getShift).setCaption("Posuv opletu");
        gridRopes.addColumn(Lano::getWallId).setCaption("ID stěny");
        gridRopes.setItems(lanoClient.findAll().getLana());

        gridRopes.addItemClickListener(e -> {
            txtEditLen.setValue(String.valueOf(e.getItem().getLenght()));
            txtEditWid.setValue(String.valueOf(e.getItem().getWidth()));
            txtEditShift.setValue(String.valueOf(e.getItem().getShift()));
            txtEditId.setValue(e.getItem().getId().toString());
            txtEditWall.setValue(e.getItem().getWallId().toString());

        });

        // grid of stena
        gridSteny.addColumn(Stena::getId).setCaption("ID");
        gridSteny.addColumn(Stena::getCity).setCaption("City");
        gridSteny.addColumn(Stena::getStreet).setCaption("Street");
        gridSteny.addColumn(Stena::getStreetNumber).setCaption("Number");
        gridSteny.setItems(stenaClient.findAll().getSteny());

        gridSteny.addItemClickListener(e -> {
            txtEditWall.setValue(e.getItem().getId().toString());
        });

        layoutGrid.addComponents(gridRopes, gridSteny);

        // add components
        layoutDelete.addComponents(btnDelete, btnAll);
        layoutLano.addComponents(layoutHome, layoutQuery, layoutDelete, layoutGrid);

        setContent(layoutLano);
    }
    
    private void MainScreen() {
        VerticalLayout layoutMain = new VerticalLayout();
        HorizontalLayout layoutButtons = new HorizontalLayout();
        Button btnTrener = new Button("Trenér");
        Button btnStena = new Button("Stěna");
        Button btnLano = new Button("Lano");
        
        btnTrener.addClickListener( e -> {
            Trener();
        });
        btnStena.addClickListener( e -> {
            Stena();
        });
        btnLano.addClickListener( e -> {
            Lano();
        });
        
        layoutButtons.addComponents(btnTrener, btnStena, btnLano);
        layoutMain.addComponent(layoutButtons);
        setContent(layoutMain);
    }
    
    private void EditTrener(String id, String first, String second, String email, String idWall) {
        if (!"".equals(id) &&
            !"".equals(first) &&
            !"".equals(second) &&
            !"".equals(idWall) &&
            !"".equals(email)) {
            Stena s;
            try {
                Long.parseLong(idWall);
                s = stenaClient.find_JSON(Stena.class, idWall);
            }
            catch (NumberFormatException | ClientErrorException e) {
                Notification.show("ID stěny musí být platné", Notification.TYPE_ERROR_MESSAGE);
                return;
            }
            Trener t = trenerClient.find_JSON(Trener.class, id);
            t.setFirstName(first);
            t.setSurname(second);
            t.setEmail(email);
            t.AddStena(s);
//            s.AddTrener(t);
            trenerClient.edit_JSON(t, id);
//            stenaClient.edit_JSON(s, idWall);
        }
        else if ("".equals(idWall)) {
            Trener t = trenerClient.find_JSON(Trener.class, id);
            t.setFirstName(first);
            t.setSurname(second);
            t.setEmail(email);
            trenerClient.edit_JSON(t, id);
        }
    }
    
    private void EditStena(String id, String city, String street, String number){
        if (!"".equals(id) &&
            !"".equals(city) &&
            !"".equals(street) &&
            !"".equals(number)) {
            try {
                int num = Integer.parseInt(number);
                stenaClient.edit_JSON(new StenaFactory(city, street, num, Long.parseLong(id)).getStena(), id);
            }
            catch (Exception e) {
                
            }
        }
    }
    
    private void EditLano(String id, String len, String wid, String shi, String idWall) {
        if (!"".equals(id) &&
            !"".equals(len) &&
            !"".equals(wid) &&
            !"".equals(shi) &&
            !"".equals(idWall)) {
            Stena s;
            try {
                Long.parseLong(idWall);
                s = stenaClient.find_JSON(Stena.class, idWall);
            }
            catch (Exception e) {
                Notification.show("ID stěny musí být platné", Notification.TYPE_ERROR_MESSAGE);
                return;
            }
            int leng;
            double widt, shift;
            try {
                leng = Integer.parseInt(len);
                widt = Double.parseDouble(wid);
                shift = Double.parseDouble(shi);
            }
            catch (Exception e) {
                Notification.show("Hodnoty musí být čísla", Notification.TYPE_ERROR_MESSAGE);
                return;
            }
            Lano l = lanoClient.find_JSON(Lano.class, id);
            l.setLenght(leng);
            l.setShift(shift);
            l.setWidth(widt);
            l.setWall(s);
            lanoClient.edit_JSON(l, l.getId().toString());
        }
    }

    private void CreateTrener(String first, String second, String email) {
        if (!"".equals(first) &&
            !"".equals(second) &&
            !"".equals(email)) {
            if (email.matches("\\S+@\\S+\\.\\S+")) {
                trenerClient.create_JSON(
                    new TrenerFactory(first, second, email).getTrener()
                );
            }
            else {
                Notification.show("Email ve špatném formátu", Notification.TYPE_ERROR_MESSAGE);
            }
        }
        else {
            Notification.show("Pole nesmějí být prázdná", Notification.TYPE_ERROR_MESSAGE);
        }
    }
    
    private void CreateLano(String length, String width, String shift, String idWall) {
        if (!"".equals(length) &&
            !"".equals(width) &&
            !"".equals(shift) &&
            !"".equals(idWall)) {
            try {
                Stena s = stenaClient.find_JSON(Stena.class, idWall);
                int len = Integer.parseInt(length);
                double wid = Double.parseDouble(width);
                double sh = Double.parseDouble(shift);
                lanoClient.create_JSON(
                    new LanoFactory(len, wid, sh, s).getLano()
                );
            }
            catch (Exception e) {
                Notification.show("Pole musejí být čísla", Notification.TYPE_ERROR_MESSAGE);
            }
        }
        else {
            Notification.show("Pole nesmějí být prázdná", Notification.TYPE_ERROR_MESSAGE);
        }
    }

    private void RemoveWall(Stena s) {
        if (lastTrener != null) {
            lastTrener.RemoveStena(s);
            trenerClient.edit_JSON(lastTrener, lastTrener.getId().toString());
        }
    }
    
    private void ChangeSelected(Trener t) {
        lastTrener = t;
    }
    
    private void CreateWall(String city, String street, String num) {
        if (!"".equals(city) &&
            !"".equals(street) &&
            !"".equals(num)) {
            try {
                int n = Integer.parseInt(num);
                stenaClient.create_JSON(
                    new StenaFactory(city, street, n).getStena()
                );
            }
            catch (Exception e) {
                Notification.show("Číslo ulice musí být přirozené číslo", Notification.TYPE_ERROR_MESSAGE);
            }
        }
        else {
            Notification.show("Pole nesmějí být prázdná", Notification.TYPE_ERROR_MESSAGE);
        }
    }
    
    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
