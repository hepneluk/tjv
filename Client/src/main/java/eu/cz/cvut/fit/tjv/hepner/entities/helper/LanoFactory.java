/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.cz.cvut.fit.tjv.hepner.entities.helper;

import eu.cz.cvut.fit.tjv.hepner.entities.Lano;
import eu.cz.cvut.fit.tjv.hepner.entities.Stena;

/**
 *
 * @author heppy
 */
public class LanoFactory {
    private Lano lano;

    public LanoFactory(int lenght, double width, double shift, Stena wall) {
        lano = new Lano();
        lano.setLenght(lenght);
        lano.setWidth(width);
        lano.setShift(shift);
        lano.setWall(wall);
    }
    
    public Lano getLano() {
        return lano;
    }
}
